<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Video;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpFoundation\Response;




class VideoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $VideoUsuario = Video::where('users_id', '=' , Auth::user()->id )->orderBy('created_at', 'desc')->paginate(6);

        return view('video.indexVideo' ,  array('videos' => $VideoUsuario));
    }

    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('video.createVideo');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validacion = $request->validate([
            'Titulo' => 'required|max:120',
            'Miniatura' => 'required|mimes:jpg,png',
            'Video' => 'required|mimetypes:video/avi,video/mpeg,video/mp4'
        ]);

        
        
        $video  = new Video();
        $user   = Auth::user();
        $titulo = $request->Titulo;

        $video->users_id    = $user->id;
        $video->title       = $titulo; 
        $video->descriotion = $request->Descripcion;
        
        $imagenFile = $request->file('Miniatura');
        $pathImagen = str_replace( array("@" , ".") , '_' , $user->email) . "_" . str_replace(" " , "_" , $titulo ) . "_" . uniqid() . ".". $imagenFile->getClientOriginalExtension();
        Storage::disk('imagen')->put($pathImagen, file_get_contents($imagenFile));

        $videofile = $request->file('Video');
        $pathVideo =  str_replace( array("@" , ".") , '_' , $user->email) . "_" . str_replace(" " , "_" , $titulo ) . "_" . uniqid() . ".". $videofile->getClientOriginalExtension();
        Storage::disk('videos')->put($pathVideo, file_get_contents($videofile));

        $video->video  = $pathVideo;
        $video->imagen = $pathImagen;

        $video->save();

        return redirect('/');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       
        $video = Video::find($id);
        $comment = $video->comments;

        return view('video.showVideo' , array('video' => $video ) );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $video = Video::find($id);
        return view('video.editVideo' , array('video' => $video) );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $video  = Video::find($id);
        $user = Auth::user();
        if($video->users_id != Auth::user()->id){
            $error = array('style' => 'alert-danger' , 'msg' => 'Error al modificar video');
            $VideoUsuario = Video::where('users_id', '=' , Auth::user()->id )->orderBy('created_at', 'desc')->paginate(6);

            return view('video.indexVideo' ,  array('videos' => $VideoUsuario , 'error' => $error));
        }

        $validacion = $request->validate([
            'Titulo' => 'max:120',
            'Miniatura' => 'mimes:jpg,png',
            'Video' => 'mimetypes:video/avi,video/mpeg,video/mp4'
        ]);

        $video->title = $request->Titulo ? $request->Titulo : $video->title;
        $video->descriotion = $request->Descripcion ? $request->Descripcion : $video->descriotion;

        $imagenFile = $request->file('Miniatura');
        if($imagenFile){
            $pathImagen = str_replace( array("@" , ".") , '_' , $user->email) . "_" . str_replace(" " , "_" , $video->title ) . "_" . uniqid() . ".". $imagenFile->getClientOriginalExtension();
            Storage::disk('imagen')->put($pathImagen, file_get_contents($imagenFile)); 
            $video->imagen = $pathImagen;
        }
        

        $videofile = $request->file('Video');
        
        if($videofile){
            $pathVideo =  str_replace( array("@" , ".") , '_' , $user->email) . "_" . str_replace(" " , "_" , $video->title ) . "_" . uniqid() . ".". $videofile->getClientOriginalExtension();
            Storage::disk('videos')->put($pathVideo, file_get_contents($videofile));
            $video->video  = $pathVideo;
        }        

        $video->save();
        
        $error = array('style' => 'alert-success' , 'msg' => 'Video Editado');
        return view('video.showVideo' , array('video' => $video , 'error' => $error ) );

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

}
