<?php

namespace App\Http\Controllers;

use App\Models\Video;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Storage;

use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

class HomeController extends Controller
{
    
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

        $video = Video::paginate(6);

        return view('home' , array('videos' => $video));
    }



    /**
     * Show the image video.
     *
     * @param string $path
     * @return Symfony\Component\HttpFoundation\Response img
     */
    
    public function miniatura($path){

        
        if (Storage::disk('imagen')->exists($path)) {
            $img = Storage::disk('imagen')->get($path);
        }else{
            $img = Storage::disk('imagen')->get('default_video.png');
        }

        

        return new Response($img , 200);
    }

    public function video($path){

        $video = Storage::disk('videos')->get($path);
        
        //,200 , array('Content-Type' => 'video/mp4')
        $response = new Response($video , 200  );

        return $response;
    }


}
