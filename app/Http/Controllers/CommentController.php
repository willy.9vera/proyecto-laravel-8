<?php

namespace App\Http\Controllers;

use App\Models\Comment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CommentController extends Controller
{
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){
        $validacion = $request->validate([
            'body' => 'required',
        ]);

        $comment = new Comment();
        $user = Auth::user();

        $comment->users_id = $user->id;
        $comment->video_id = $request->input('video_id');
        $comment->body = $request->input('body');

        $comment->save();

        $error = array('style' => 'alert-success' , 'msg' => 'Se agrego el comentario correctamente!!');
        return redirect()->route('video.show', ['video' => $comment->video_id])->with(array('error' => $error ));

    }

}
