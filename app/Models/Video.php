<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Video extends Model
{
    //use HasFactory;

    protected $table = "video";

    
    public function comments(){
        return $this->hasMany(Comment::class , 'video_id' );
    }
    public function user(){
        return $this->belongsTo(User::class , 'users_id');
    }
}
