<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{

    protected $table = "comments";

    public function video(){
        return $this->belongsTo(Video::class , 'video_id');
    }
    public function user(){
        return $this->belongsTo(User::class , 'users_id');
    }
}
