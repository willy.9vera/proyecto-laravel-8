@extends('layouts.app')

@section('content')
<div class="container"> 

  <div class="row row-cols-1 row-cols-md-3 g-4">
    

    @foreach ($videos as $video)
        
      <div class="col">
        <div class="card">
          <a href="{{url('video/' . $video->id )}}" style="object-fit: cover;"> 
            <img src="{{ url('imagendevideo/' . $video->imagen )}}" class="img-card card-img-top" >
          </a>
          <div class="card-body ">
            <p class="card-title text-truncate " data-bs-toggle="tooltip" data-bs-placement="top" title="{{$video->title }}">{{$video->title }}</p>
          </div>
        </div>
      </div>

    @endforeach

  </div>
  <br>
  {{$videos->links()}}
</div>
@endsection
