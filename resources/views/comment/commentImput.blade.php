<div class="container">

    @if (Auth::check())
        <div class="card">
            <div class="card-body">
                <form action="{{ route('comment') }}" method="POST">
                    @csrf
                    <input type="hidden" name="video_id" value="{{ $video->id }}" />

                    <div class="form-group">
                        <label for="exampleFormControlTextarea1">Comentario</label>
                        <textarea class="form-control" id="exampleFormControlTextarea1" rows="3"
                            name="body"></textarea>
                    </div>
                    <br>
                    <button type="submit" class="btn btn-primary" btn-lg btn-block>Comentar</button>
                </form>
            </div>
        </div>
    @else
        <div class="alert alert-warning alert-dismissible fade show" role="alert">
            <div class="d-flex">
                <div class="p-2">
                    <h4 class="alert-heading">No estas logeado</h4>
                </div>
                <div class="ml-auto p-2">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </div>


            <p>Inicia seción para poder hacer un comentario.</p>

        </div>
    @endif



    @if (isset($video->comments))

        @foreach ($video->comments as $c)
            <div class="card mt-3">
                <div class="card-header">
                    {{ $c->user->name }}
                </div>
                <div class="card-body">

                    <p class="card-text">{{ $c->body }}</p>

                </div>
            </div>
        @endforeach




    @endif

</div>
