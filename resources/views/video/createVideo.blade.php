@extends('layouts.app')



@section('content')




<div class="container">
    <div class="row">
        <h2>Crear video</h2>

        <form action="{{url('/video')}}" enctype="multipart/form-data" class="col-lg-7" method="post">
            
            @csrf

            @if($errors->any())
                
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $error)
                        <li>{{$error}}</li>
                    @endforeach
                </ul>
            </div>

            @endif

            <div class="form-group">
              <label for="Titulo">Titulo</label>
              <input type="text" name="Titulo" id="Titulo" class="form-control" placeholder="Titulo del Video">
            </div>

            <div class="form-group">
              <label for="Descripcion">Descripción</label>
              <textarea class="form-control" name="Descripcion" id="Descripcion"></textarea>
            </div>


            <div class="form-group">
                <label for="Miniatura">Miniatura</label>
                <input type="file" name="Miniatura" id="Miniatura" class="form-control"  >
            </div>

            <div class="form-group">
                <label for="Video">Video</label>
                <input type="file" name="Video" id="Video" class="form-control"  >
            </div>

            <br>
            
            <button type="submit" class="btn btn-primary">Subir Video</button>

        </form>

    </div>
</div>



</div>

@endsection


