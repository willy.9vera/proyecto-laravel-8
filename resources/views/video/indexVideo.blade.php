@extends('layouts.app')

@section('content')

<div class="container">


    @if(isset($error))
    <div class="alert {{$error['style']}}" role="alert">
        {{$error['msg']}}
    </div>

    @endif

    <h4>Videos subidos</h4>

    <hr>

    @if($videos->count() <= 0 )
        <div class="alert alert-warning" >
            <h4 class="alert-heading">No tienes videos!</h4>
            <p>ingresa al formulario para subir videos en la plataforma</p>
            <hr>
            <a class="btn btn-warning" href="{{url('video/create')}}" role="button">Subir video</a>
        </div>
    @endif

    @foreach ($videos as $video)
     
    <div class="card mt-4">
        
        <div class="card-body">
            <div class="row">
                <div class="col-md-4 img-card-list">

                    <div class="img-card d-flex justify-content-center " style="object-fit: cover;">
                        <img src="{{url('imagendevideo/'  .  $video->imagen)}}" class="img-fluid rounded-start ">
                    </div>
                    
                </div>
                <div class="col-md-8 ">
                    <div class="d-flex align-self-stretch flex-column ">
                        <h4 class="card-title p-2">{{$video->title}}</h4>
                        <p class="card-text p-2">{{$video->descriotion}}</p>
                        <div class="mt-auto p-2" >
                            <a href="#" class="btn btn-primary">Eliminar</a>
                            <a href="{{url('/video/'.$video->id.'/edit')}}" class="btn btn-primary">Editar</a>
                            <a href="{{url('/video/'.$video->id)}} " class="btn btn-primary">Ver video</a>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
         
    </div>

    @endforeach

   
    <br>
    {{$videos->links()}}

</div>


@endsection
