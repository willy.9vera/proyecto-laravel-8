@extends('layouts.app')

@section('content')

<div class="container">

    @if(isset($error))
    <div class="alert {{$error['style']}}" role="alert">
        {{$error['msg']}}
    </div>
    @endif

    <div class="row">
        <div class="col-md-12">
            <div  class=" embed-responsive embed-responsive-21by9">
                <video  id="video" autoplay  controls='true'>
                    <source  src="{{ route('videocrs' , ['path' => $video->video ])}}" >
                </video>
            </div>

            <br>

            <h4>{{$video->title}}</h4>
            <p>{{$video->user->name}} <small>{{FormatTime::LongTimeFilter($video->created_at)}}</small></p>
           
        </div>
    </div>



    
    <div class="row">
        
        
        @if(is_null($video->descriotion))
        
        <p>No tiene descripción</p>

        @else
        <p>
        <button class="btn btn-primary" type="button" data-bs-toggle="collapse" data-bs-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
            Descripción
        </button>
        </p>
        <div class="collapse" id="collapseExample">
            <div class="card card-body">
              {{$video->descriotion}}
            </div>
        </div>

        @endif
        
    </div>
    <hr>
    <br>

    <div class="row">
            
         @include('../comment/commentImput')
        
    </div>

</div>






@endsection