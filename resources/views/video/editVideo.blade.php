@extends('layouts.app')



@section('content')




<div class="container">
    <div class="row">
        <h2>Crear video</h2>

        <form action="{{route('video.update', ['video' => $video->id ]) }}" enctype="multipart/form-data" class="col-lg-7" method="POST">
            @method('PUT')
            @csrf
            
            @if($errors->any())
                
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $error)
                        <li>{{$error}}</li>
                    @endforeach
                </ul>
            </div>

            @endif


            

            <div class="form-group">
              <label for="Titulo">Titulo</label>
              <input type="text" name="Titulo" id="Titulo" class="form-control" placeholder="Titulo del Video" @if(isset($video)) value="{{$video->title}}"  @endif>
            </div>

            <div class="form-group">
              <label for="Descripcion">Descripción</label>
              <textarea class="form-control" name="Descripcion" id="Descripcion">@if(isset($video )) {{$video->descriotion}}  @endif</textarea>
            </div>

            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="Miniatura">Miniatura</label>
                        <input type="file" name="Miniatura" id="Miniatura" class="form-control">
                        <br>
                        <img src="{{url('imagendevideo/'  .  $video->imagen)}}" class="img-fluid rounded-start ">
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="Video">Video</label>
                        <input type="file" name="Video" id="Video" class="form-control"  >
                        <br>
                        <div  class=" embed-responsive embed-responsive-21by9">
                            <video  id="video" autoplay  controls='true'>
                                <source  src="{{ route('videocrs' , ['path' => $video->video ])}}" >
                            </video>
                        </div>
                    </div>
                </div>
            </div>
            <br>
            
            <button type="submit" class="btn btn-primary">Editar Video</button>

        </form>

    </div>
</div>



</div>

@endsection
