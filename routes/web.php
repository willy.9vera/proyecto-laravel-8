<?php

use App\Http\Controllers\CommentController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\VideoController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/home', function () {
    return redirect('/');
});


Route::get('/', [HomeController::class, 'index']);
Route::get('/imagendefaulttvideo' , [HomeController::class, 'imgDefault']);
Route::get('/imagendevideo/{path}' , [HomeController::class, 'miniatura']);
Route::get('/videocrs/{path}' , [HomeController::class, 'video'])->name('videocrs');

Route::post('/comment', [CommentController::class,'store'])->name('comment');


Auth::routes();



Route::resource('video' , VideoController::class ,['except' => [
    'show'
]])->middleware(['auth']);

Route::resource('video' , VideoController::class ,['only' => [
    'show'
]]);
